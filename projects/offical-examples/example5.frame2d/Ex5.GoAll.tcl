
puts " --  Elastic Model: Elastic Uniaxial Section, Nonlinear Element --"
puts " --  Static Pushover Analysis --"
source Ex5.Frame2D.build.ElasticSection.tcl
source Ex5.Frame2D.analyze.Static.Push.tcl

puts " --  Elastic Model: Elastic Uniaxial Section, Nonlinear Element --"
puts " --  Static Reversed-Cyclic Analysis --"
source Ex5.Frame2D.build.ElasticSection.tcl
source Ex5.Frame2D.analyze.Static.Cycle.tcl

puts " --  Elastic Model: Elastic Uniaxial Section, Nonlinear Element --"
puts " --  Uniform Sine-wave Excitation --"
source Ex5.Frame2D.build.ElasticSection.tcl
source Ex5.Frame2D.analyze.Dynamic.sine.Uniform.tcl

puts " --  Elastic Model: Elastic Uniaxial Section, Nonlinear Element --"
puts " --  Uniform Earthquake Excitation --"
source Ex5.Frame2D.build.ElasticSection.tcl
source Ex5.Frame2D.analyze.Dynamic.EQ.Uniform.tcl

puts " --  Elastic Model: Elastic Uniaxial Section, Nonlinear Element --"
puts " --  Multiple-support Sine-wave Excitation --"
source Ex5.Frame2D.build.ElasticSection.tcl
source Ex5.Frame2D.analyze.Dynamic.sine.multipleSupport.tcl

puts " --  Elastic Model: Elastic Uniaxial Section, Nonlinear Element --"
puts " --  Multiple-support Earthquake Excitation --"
source Ex5.Frame2D.build.ElasticSection.tcl
source Ex5.Frame2D.analyze.Dynamic.EQ.multipleSupport.tcl

puts " --  Elastic Model: Elastic Uniaxial Section, Nonlinear Element --"
puts " --  Bidirectional Earthquake Excitation --"
source Ex5.Frame2D.build.ElasticSection.tcl
source Ex5.Frame2D.analyze.Dynamic.EQ.bidirect.tcl

puts " --  -----------------------------------------------------------------"

puts " --  Uniaxial Inelastic Section, Nonlinear Model --"
puts " --  Static Pushover Analysis --"
source Ex5.Frame2D.build.InelasticSection.tcl
source Ex5.Frame2D.analyze.Static.Push.tcl

puts " --  Uniaxial Inelastic Section, Nonlinear Model --"
puts " --  Static Reversed-Cyclic Analysis --"
source Ex5.Frame2D.build.InelasticSection.tcl
source Ex5.Frame2D.analyze.Static.Cycle.tcl

puts " --  Uniaxial Inelastic Section, Nonlinear Model --"
puts " --  Uniform Sine-wave Excitation --"
source Ex5.Frame2D.build.InelasticSection.tcl
source Ex5.Frame2D.analyze.Dynamic.sine.Uniform.tcl

puts " --  Uniaxial Inelastic Section, Nonlinear Model --"
puts " --  Uniform Earthquake Excitation --"
source Ex5.Frame2D.build.InelasticSection.tcl
source Ex5.Frame2D.analyze.Dynamic.EQ.Uniform.tcl

puts " --  Uniaxial Inelastic Section, Nonlinear Model --"
puts " --  Multiple-support Sine-wave Excitation --"
source Ex5.Frame2D.build.InelasticSection.tcl
source Ex5.Frame2D.analyze.Dynamic.sine.multipleSupport.tcl

puts " --  Uniaxial Inelastic Section, Nonlinear Model --"
puts " --  Multiple-support Earthquake Excitation --"
source Ex5.Frame2D.build.InelasticSection.tcl
source Ex5.Frame2D.analyze.Dynamic.EQ.multipleSupport.tcl

puts " --  Uniaxial Inelastic Section, Nonlinear Model --"
puts " --  Bidirectional Earthquake Excitation --"
source Ex5.Frame2D.build.InelasticSection.tcl
source Ex5.Frame2D.analyze.Dynamic.EQ.bidirect.tcl

puts " --  ----------------------------------------------------------------- "

puts " --  Uniaxial Inelastic Material, Fiber W-Section, Nonlinear Model --"
puts " --  Static Pushover Analysis --"
source Ex5.Frame2D.build.InelasticFiberWSection.tcl
source Ex5.Frame2D.analyze.Static.Push.tcl

puts " --  Uniaxial Inelastic Material, Fiber W-Section, Nonlinear Model --"
puts " --  Static Reversed-Cyclic Analysis --"
source Ex5.Frame2D.build.InelasticFiberWSection.tcl
source Ex5.Frame2D.analyze.Static.Cycle.tcl

puts " --  Uniaxial Inelastic Material, Fiber W-Section, Nonlinear Model --"
puts " --  Uniform Sine-wave Excitation --"
source Ex5.Frame2D.build.InelasticFiberWSection.tcl
source Ex5.Frame2D.analyze.Dynamic.sine.Uniform.tcl

puts " --  Uniaxial Inelastic Material, Fiber W-Section, Nonlinear Model --"
puts " --  Uniform Earthquake Excitation --"
source Ex5.Frame2D.build.InelasticFiberWSection.tcl
source Ex5.Frame2D.analyze.Dynamic.EQ.Uniform.tcl

puts " --  Uniaxial Inelastic Material, Fiber W-Section, Nonlinear Model --"
puts " --  Multiple-support Sine-wave Excitation --"
source Ex5.Frame2D.build.InelasticFiberWSection.tcl
source Ex5.Frame2D.analyze.Dynamic.sine.multipleSupport.tcl

puts " --  Uniaxial Inelastic Material, Fiber W-Section, Nonlinear Model --"
puts " --  Multiple-support Earthquake Excitation --"
source Ex5.Frame2D.build.InelasticFiberWSection.tcl
source Ex5.Frame2D.analyze.Dynamic.EQ.multipleSupport.tcl

puts " --  Uniaxial Inelastic Material, Fiber W-Section, Nonlinear Model --"
puts " --  Bidirectional Earthquake Excitation --"
source Ex5.Frame2D.build.InelasticFiberWSection.tcl
source Ex5.Frame2D.analyze.Dynamic.EQ.bidirect.tcl


puts " --  ----------------------------------------------------------------- "

puts " --  Uniaxial Inelastic Material, Fiber RC-Section, Nonlinear Model --"
puts " --  Static Pushover Analysis --"
source Ex5.Frame2D.build.InelasticFiberRCSection.tcl
source Ex5.Frame2D.analyze.Static.Push.tcl

puts " --  Uniaxial Inelastic Material, Fiber RC-Section, Nonlinear Model --"
puts " --  Static Reversed-Cyclic Analysis --"
source Ex5.Frame2D.build.InelasticFiberRCSection.tcl
source Ex5.Frame2D.analyze.Static.Cycle.tcl

puts " --  Uniaxial Inelastic Material, Fiber RC-Section, Nonlinear Model --"
puts " --  Uniform Sine-Wave Excitation --"
source Ex5.Frame2D.build.InelasticFiberRCSection.tcl
source Ex5.Frame2D.analyze.Dynamic.sine.Uniform.tcl

puts " --  Uniaxial Inelastic Material, Fiber RC-Section, Nonlinear Model --"
puts " --  Uniform Earthquake Excitation --"
source Ex5.Frame2D.build.InelasticFiberRCSection.tcl
source Ex5.Frame2D.analyze.Dynamic.EQ.Uniform.tcl

puts " --  Uniaxial Inelastic Material, Fiber RC-Section, Nonlinear Model --"
puts " --  Multiple-support Sine-Wave Excitation --"
source Ex5.Frame2D.build.InelasticFiberRCSection.tcl
source Ex5.Frame2D.analyze.Dynamic.sine.multipleSupport.tcl

puts " --  Uniaxial Inelastic Material, Fiber RC-Section, Nonlinear Model --"
puts " --  Multiple-support Earthquake Excitation --"
source Ex5.Frame2D.build.InelasticFiberRCSection.tcl
source Ex5.Frame2D.analyze.Dynamic.EQ.multipleSupport.tcl

puts " --  Uniaxial Inelastic Material, Fiber RC-Section, Nonlinear Model --"
puts " --  Bidirectional Earthquake Excitation --"
source Ex5.Frame2D.build.InelasticFiberRCSection.tcl
source Ex5.Frame2D.analyze.Dynamic.EQ.bidirect.tcl


