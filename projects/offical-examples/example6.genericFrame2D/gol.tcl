puts " --  ----------------------------------------------------------------- "

puts " --  Inelastic Uniaxial Section, Nonlinear Model --"
puts " --  Uniform Sine-wave Excitation --"
source Ex6.genericFrame2D.build.InelasticSection.tcl
source Ex6.genericFrame2D.analyze.Dynamic.sine.Uniform.tcl

puts " --  Inelastic Uniaxial Section, Nonlinear Model --"
puts " --  Multiple-support Sine-wave Excitation --"
source Ex6.genericFrame2D.build.InelasticSection.tcl
source Ex6.genericFrame2D.analyze.Dynamic.sine.multipleSupport.tcl

puts " --  Uniaxial Inelastic Material, Fiber W-Section, Nonlinear Model --"
puts " --  Uniform Sine-wave Excitation --"
source Ex6.genericFrame2D.build.InelasticFiberWSection.tcl
source Ex6.genericFrame2D.analyze.Dynamic.sine.Uniform.tcl

puts " --  Uniaxial Inelastic Material, Fiber W-Section, Nonlinear Model --"
puts " --  Multiple-support Sine-wave Excitation --"
source Ex6.genericFrame2D.build.InelasticFiberWSection.tcl
source Ex6.genericFrame2D.analyze.Dynamic.sine.multipleSupport.tcl

puts " --  Uniaxial Inelastic Material, Fiber RC-Section, Nonlinear Model --"
puts " --  Uniform Sine-wave Excitation --"
source Ex6.genericFrame2D.build.InelasticFiberRCSection.tcl
source Ex6.genericFrame2D.analyze.Dynamic.sine.Uniform.tcl

puts " --  Uniaxial Inelastic Material, Fiber RC-Section, Nonlinear Model --"
puts " --  Multiple-support Sine-wave Excitation --"
source Ex6.genericFrame2D.build.InelasticFiberRCSection.tcl
source Ex6.genericFrame2D.analyze.Dynamic.sine.multipleSupport.tcl
