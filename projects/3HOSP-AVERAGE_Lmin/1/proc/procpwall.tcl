## lumping script for four elements in parallel
## Richard Wood 
## UC San Diego
## May 26, 2009

proc procpwall { matID pStress nStress pEnvelopeStrain nEnvelopeStrain rD rF} {

############################# General Terms #######################################################
set uForce [list -0.01 -0.01]
set gammaK [list -100.0 -100.0 -100.0 -100.0 -1000.0 ]
set gammaD [list 0.0 0.0 0.0 0.0 0.0]
set gammaD [list 0.0 0.0 0.0 0.0 0.0]
set gammaF [list 0.0 0.0 0.0 0.0 0.0]
set gammaE 100.0;
set damage "cycle";
############################# Material 1 #######################################################
set matID1 9001;
set pEnvelopeStress [list [expr [lindex $pStress 0]] 0.001 0.0011 0.0012]
set nEnvelopeStress [list [expr [lindex $nStress 0]] -0.001 -0.0011 -0.0012]
set rDisp [list [expr [lindex $rD 0]] [expr [lindex $rD 0]]]
set rForce [list [expr [lindex $rF 0]] [expr [lindex $rF 0]]]
set gammaK [list -10000.0 -10000.0 -10000.0 -10000.0 -100000.0 ]
uniaxialMaterial Pinching4 $matID1 [lindex $pEnvelopeStress 0] [lindex $pEnvelopeStrain 0] [lindex $pEnvelopeStress 1] [lindex $pEnvelopeStrain 1] [lindex $pEnvelopeStress 2] [lindex $pEnvelopeStrain 2] [lindex $pEnvelopeStress 3] [lindex $pEnvelopeStrain 3] [lindex $nEnvelopeStress 0] [lindex $nEnvelopeStrain 0] [lindex $nEnvelopeStress 1] [lindex $nEnvelopeStrain 1] [lindex $nEnvelopeStress 2] [lindex $nEnvelopeStrain 2] [lindex $nEnvelopeStress 3] [lindex $nEnvelopeStrain 3] [lindex $rDisp 0] [lindex $rForce 0] [lindex $uForce 0] [lindex $rDisp 1] [lindex $rForce 1] [lindex $uForce 1] [lindex $gammaK 0] [lindex $gammaK 1] [lindex $gammaK 2] [lindex $gammaK 3] [lindex $gammaK 4] [lindex $gammaD 0] [lindex $gammaD 1] [lindex $gammaD 2] [lindex $gammaD 3] [lindex $gammaD 4] [lindex $gammaF 0] [lindex $gammaF 1] [lindex $gammaF 2] [lindex $gammaF 3] [lindex $gammaF 4] $gammaE $damage
############################# Material 2 #######################################################
set matID2 9002;
set pEnvelopeStress [list 0.001 [expr [lindex $pStress 1]] 0.001 0.0011]
set nEnvelopeStress [list -0.001 [expr [lindex $nStress 1]] -0.001 -0.0011]
set rDisp [list [expr [lindex $rD 1]] [expr [lindex $rD 1]]]
set rForce [list [expr [lindex $rF 1]] [expr [lindex $rF 1]]]
uniaxialMaterial Pinching4 $matID2 [lindex $pEnvelopeStress 0] [lindex $pEnvelopeStrain 0] [lindex $pEnvelopeStress 1] [lindex $pEnvelopeStrain 1] [lindex $pEnvelopeStress 2] [lindex $pEnvelopeStrain 2] [lindex $pEnvelopeStress 3] [lindex $pEnvelopeStrain 3] [lindex $nEnvelopeStress 0] [lindex $nEnvelopeStrain 0] [lindex $nEnvelopeStress 1] [lindex $nEnvelopeStrain 1] [lindex $nEnvelopeStress 2] [lindex $nEnvelopeStrain 2] [lindex $nEnvelopeStress 3] [lindex $nEnvelopeStrain 3] [lindex $rDisp 0] [lindex $rForce 0] [lindex $uForce 0] [lindex $rDisp 1] [lindex $rForce 1] [lindex $uForce 1] [lindex $gammaK 0] [lindex $gammaK 1] [lindex $gammaK 2] [lindex $gammaK 3] [lindex $gammaK 4] [lindex $gammaD 0] [lindex $gammaD 1] [lindex $gammaD 2] [lindex $gammaD 3] [lindex $gammaD 4] [lindex $gammaF 0] [lindex $gammaF 1] [lindex $gammaF 2] [lindex $gammaF 3] [lindex $gammaF 4] $gammaE $damage
############################# Material 3 #######################################################
set matID3 9003;
set pEnvelopeStress [list 0.001 0.0011 [expr [lindex $pStress 2]] 0.001]
set nEnvelopeStress [list -0.001 -0.0011 [expr [lindex $nStress 2]] -0.001]
set rDisp [list [expr [lindex $rD 2]] [expr [lindex $rD 2]]]
set rForce [list [expr [lindex $rF 2]] [expr [lindex $rF 2]]]
uniaxialMaterial Pinching4 $matID3 [lindex $pEnvelopeStress 0] [lindex $pEnvelopeStrain 0] [lindex $pEnvelopeStress 1] [lindex $pEnvelopeStrain 1] [lindex $pEnvelopeStress 2] [lindex $pEnvelopeStrain 2] [lindex $pEnvelopeStress 3] [lindex $pEnvelopeStrain 3] [lindex $nEnvelopeStress 0] [lindex $nEnvelopeStrain 0] [lindex $nEnvelopeStress 1] [lindex $nEnvelopeStrain 1] [lindex $nEnvelopeStress 2] [lindex $nEnvelopeStrain 2] [lindex $nEnvelopeStress 3] [lindex $nEnvelopeStrain 3] [lindex $rDisp 0] [lindex $rForce 0] [lindex $uForce 0] [lindex $rDisp 1] [lindex $rForce 1] [lindex $uForce 1] [lindex $gammaK 0] [lindex $gammaK 1] [lindex $gammaK 2] [lindex $gammaK 3] [lindex $gammaK 4] [lindex $gammaD 0] [lindex $gammaD 1] [lindex $gammaD 2] [lindex $gammaD 3] [lindex $gammaD 4] [lindex $gammaF 0] [lindex $gammaF 1] [lindex $gammaF 2] [lindex $gammaF 3] [lindex $gammaF 4] $gammaE $damage
############################# Material 4 #######################################################
set matID4 9004;
set pEnvelopeStress [list 0.001 0.0011 0.0012 [expr [lindex $pStress 3]]]
set nEnvelopeStress [list -0.001 -0.0011 -0.0012 [expr [lindex $nStress 3]] ]
set rDisp [list [expr [lindex $rD 3]] [expr [lindex $rD 3]]]
set rForce [list [expr [lindex $rF 3]] [expr [lindex $rF 3]]]
uniaxialMaterial Pinching4 $matID4 [lindex $pEnvelopeStress 0] [lindex $pEnvelopeStrain 0] [lindex $pEnvelopeStress 1] [lindex $pEnvelopeStrain 1] [lindex $pEnvelopeStress 2] [lindex $pEnvelopeStrain 2] [lindex $pEnvelopeStress 3] [lindex $pEnvelopeStrain 3] [lindex $nEnvelopeStress 0] [lindex $nEnvelopeStrain 0] [lindex $nEnvelopeStress 1] [lindex $nEnvelopeStrain 1] [lindex $nEnvelopeStress 2] [lindex $nEnvelopeStrain 2] [lindex $nEnvelopeStress 3] [lindex $nEnvelopeStrain 3] [lindex $rDisp 0] [lindex $rForce 0] [lindex $uForce 0] [lindex $rDisp 1] [lindex $rForce 1] [lindex $uForce 1] [lindex $gammaK 0] [lindex $gammaK 1] [lindex $gammaK 2] [lindex $gammaK 3] [lindex $gammaK 4] [lindex $gammaD 0] [lindex $gammaD 1] [lindex $gammaD 2] [lindex $gammaD 3] [lindex $gammaD 4] [lindex $gammaF 0] [lindex $gammaF 1] [lindex $gammaF 2] [lindex $gammaF 3] [lindex $gammaF 4] $gammaE $damage
############################# Lumped Material #######################################################
uniaxialMaterial Parallel $matID $matID1 $matID2 $matID3 $matID4

}