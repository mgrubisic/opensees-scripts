# Static Pushover ----------------------------------------------------------------------------------------------------------
puts "Pushover Analysis"
set topnode 12;

recorder Node -file $dir/roof_disp.txt -time -node $topnode -dof 1 disp;

set Flat 0.05;

set Flat1 [expr $Flat*0.166666];		# nodal distribtuion of lateral forces....
set Flat2 [expr $Flat*0.333333];
set Flat3 [expr $Flat*0.50];


pattern Plain 2 Linear {
   load 4 $Flat1 0.0 0.0;			# node#, FX FY MZ -- representative lateral load at top node
   load 7 $Flat2 0.0 0.0;	
   load 10 $Flat3 0.0 0.0;	
	};

# pushover: diplacement controlled static analysis
integrator DisplacementControl $topnode 1  0.0936;		# switch to displacement control, for node 19, dof 1, 0.02 increment
test NormDispIncr 1.0e-4 9800;
algorithm KrylovNewton;
analyze 200

puts "Pushover Finished!"
