# --------------------------------------------------------------------------------------------------
# script by Richard Wood
# units are presribed as: kip, inch, second
# see accompanying Risa Figure for details
# using Newmark's integration method;
# alpha = 0.5;
# beta = 0.25;
 
# SET UP ----------------------------------------------------------------------------
wipe analysis;
wipe;						# clearing opensees model
model basic -ndm 2 -ndf 3;				# 2 dimensions, 3 dof per node
source input_parameters.txt
file mkdir $dir; 					# create data directory
puts $dir
puts "3 SAC File Loaded - working..."

# define variables-------------------------------------------------------------
set damping 0.05;
set g 386.4;


# define GEOMETRY -------------------------------------------------------------


# Define ELEMENTS -------------------------------------------------------------
geomTransf Corotational 1;

#source input_parameters.txt
#set dir config_a ;
source building_files/nodal_b.txt

# Single point constraints -- Boundary Conditions
fix 1 1 1 1; 			# node DX DY RZ (node 1)
fix 2 1 1 1;			# fixing node 2 as well
fix 3 1 1 1;

source building_files/material.txt ;
source building_files/conn.txt

				# create data directory

source building_files/mass.txt ;


# Define Partition Wall Elements ----------------------------------------------------------------


set PW 1
set IDPW 0
set PWType 1
set lenwall 43.0
set partition_call partitions_b.txt


if {$PW == 1} {
set matID 1000;
source proc/pwall3.tcl
source building_files/partitions_b.txt  ;

puts $lenwall;
} else {
  puts "NO PW WALL";    
}

# Define RECORDERS ----------------------------------------------------------------------------------------------------
recorder Node -file $dir/reacts.txt -node 1 2 3 -dof 1 2 3 reaction;
recorder Node -file $dir/reacts.txt -node 4 5 6 -dof 1 2 3 reaction;

recorder Node -file $dir/floor_acc.txt -time -dT 0.01 -node 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 -dof 1 accel;
recorder Node -file $dir/floor_disp.txt -time -dT 0.01 -node 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 -dof 1 disp;
recorder Node -file $dir/floor_rot.txt -time -dT 0.01 -node 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 -dof 3 disp;

#recorder Node -file $dir/pw_acc.txt -time -dT 0.01 -node 101 102 201 202 301 302 401 402 501 502 601 602 701 702 801 802 -dof 1 accel;
#recorder Node -file $dir/pw_disp.txt -time -dT 0.01 -node 101 102 201 202 301 302 401 402 501 502 601 602 701 702 801 802 -dof 1 disp;
#recorder Node -file $dir/pw_rot.txt -time -dT 0.01 -node 101 102 201 202 301 302 401 402 501 502 601 602 701 702 801 802 -dof 3 disp;

#recorder Element -file $dir/partion_forces.txt -time -dT 0.01 -ele 1001 1002 1003 1004 1005 1006 1007 1008 force;

recorder Node -file $dir/fl_disp.txt -time -dT 0.01 -node 2 5 8 11 14 17 20 23 26 29 32 35 38 41 44 47 50 53 56 59 62 -dof 1 disp;
logFile $dir/log.txt

# determing Eigenvalues --------------------------------------------------------------------------------------------------
set outfile [open $dir/eigenvalues.txt w]
set eigenvalues [eigen 3]
puts $outfile $eigenvalues
close $outfile 
# determing EigenVectors -------------------------------------------------------------------------------------------------
source building_files/eigen.txt
 
# Define Gravity --------------------------------------------------------------------------------------------------------
pattern Plain 1 Linear {source building_files/gravity.txt };

#constraints Plain;  
constraints Transformation;     				#  b.c.s
numberer Plain;					# no renumbering
system BandGeneral;				# system of equation storage, not for efficiency here
test NormDispIncr 1.0 20; 				# convergence check 
algorithm Newton;					# use Newton's solution algorithm: updates tangent stiffness at every iteration
integrator LoadControl 0.1;				# determine the next time step for an analysis, load applied in multiples of 0.10
analysis Static					# defining static analysis
analyze 10;					# perform gravity analysis to reach 1.0
loadConst -time 0.0;				# hold gravity cnstant, reset time and then apply dynamic loading

puts $dir
puts "Finished!"

# puts "File loading, running analysis now"
# source analysis.tcl
