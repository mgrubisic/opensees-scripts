set dt 0.02
set input_motion gmotion.txt
timeSeries Path 1 -dt $dt -filePath $input_motion -factor $g
pattern UniformExcitation  10 1 -accel  1;		

constraints Transformation;
numberer Plain
system BandGeneral;	
set  TolDynamic 1.e-8;                  
set  maxNumIterDynamic 10;              
set  printFlagDynamic 0;                
set  testTypeDynamic EnergyIncr;	
test $testTypeDynamic $TolDynamic $maxNumIterDynamic $printFlagDynamic;
algorithm Newton;        
set NewmarkGamma 0.5;
set NewmarkBeta 0.25;
set integratorTypeDynamic Newmark;
integrator $integratorTypeDynamic $NewmarkGamma $NewmarkBeta
analysis Transient
set Nsteps 2500
set ok [analyze $Nsteps $dt]