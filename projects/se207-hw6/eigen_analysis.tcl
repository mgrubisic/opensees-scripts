set lambda [eigen 2];
set pi 3.1415927

set w1 [expr pow([lindex $lambda 0],0.5)];
set T1 [expr 2*$pi/$w1]
puts "w1 = $w1, T1 = $T1"

set w2 [expr pow([lindex $lambda 1],0.5)];
set T2 [expr 2*$pi/$w2]
puts "w2 = $w2, T2 = $T2"

set dratio 0.02
set a1 [expr  2*$dratio/($w1+$w2)];
set a0 [expr  2*$dratio*$w1*$w2/($w1+$w2)];
puts "a0=$a0, a1=$a1"

                  