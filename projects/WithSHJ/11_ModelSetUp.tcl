#  ProjectName: California High Speed RailWay System.
#  Define the load information based on CHSR
#  Developed by Yong LI, at University of California, San Diego
#  2012. March. 8th, foxchameleon@gmail.com

set unitsSys "US"; # SI, US
if {$unitsSys == "SI"} {
		source SIUnits.tcl;
	} else {
		source BISUnits.tcl;
	}
	
# foreach { LCTYPE } { "LC_GravityAlone" "LC_Strength1_1" "LC_Strength2_1" "LC_Strength3_1" "LC_Strength4_1" "LC_Strength5_1" "LC_Strength5_2"  \
                     # "LC_Extreme1_1" "LC_Extreme2_1" "LC_Extreme3_1"  "LC_Extreme3_2"   "LC_Service1_1" "LC_Service2_1" "LC_Service3_1" } {
	# source SystemPara_Yong5.tcl;
# }

# foreach { LCTYPE } { "LC_Group_1a" "LC_Group_1b" "LC_Group_1c" "LC_Group_2_1" "LC_Group_3_1" "LC_Group_3_2" } {
	# source SystemPara_Yong5.tcl;
# }

# foreach { LCTYPE } { "LC_Group_4_1" "LC_Group_5_1" "LC_Group_5_2" } {
	# source SystemPara_Yong5.tcl;
# }


foreach { LCTYPE } { "LC_GravityAlone" } {
	source SystemPara_Yong6.tcl;
}