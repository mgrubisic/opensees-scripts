#Output Nodes Information
set dataDir .;
set NodesDataFile [open "$dataDir/NodesDataFile.out" "w"];
set NodeTags [getNodeTags];
set numNodes [llength $NodeTags];
puts "The Number of Nodes in the Model: $numNodes"
for { set inode 1} { $inode <= $numNodes } { incr inode 1} {
	set nodei [lindex $NodeTags [expr $inode-1]]
	set aX [nodeCoord $nodei 1]
	set aY [nodeCoord $nodei 2]
	set aZ [nodeCoord $nodei 3]
	#puts "node $nodei $aX $aY $aZ  "
	puts -nonewline $NodesDataFile "\n";
	set NodesData "node $nodei $aX $aY $aZ";
	puts -nonewline $NodesDataFile $NodesData;
}
close $NodesDataFile;

#Output Elements Information
set ElementsDataFile [open "$dataDir/ElementsDataFile.out" "w"];
set EleTags [getEleTags];
set numEle [llength $EleTags];
puts "The Number of Elements in the Model: $numEle"
set ele1 [lindex $ElementsDataFile 0];
for { set iele 1} { $iele <= $numEle } { incr iele 1} {
	set elei [lindex $EleTags [expr $iele-1]]
	set nodes_ele [eleNodes $elei]
	#puts "element $elei $bele  "
	puts -nonewline $ElementsDataFile "\n";
	set ElementsData "element $elei $nodes_ele";
	puts -nonewline $ElementsDataFile $ElementsData;
}
close $ElementsDataFile;

