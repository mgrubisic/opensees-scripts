# Note: Analyze.EQ.TimeHisotory.ConvergenceOpt.tcl is to improve the convergence capacity
# Modified By Yong Li, August 2010, Univerisity of California at San Diego. 
# Contact: foxchameleon@gmail.com / yongli@ucsd.edu
####################################################################################################
# Parameters Initialization for solution parameters: Added by Yong Li,2012.01.18
set constraintsTypeDynamicNew $constraintsTypeDynamic;
set numbererTypeDynamicNew $numbererTypeDynamic;
set systemTypeDynamicNew $systemTypeDynamic;
set testTypeDynamicNew $testTypeDynamic;
set TolDynamicNew $TolDynamic;
set maxNumIterDynamicNew $maxNumIterDynamic
set algorithmTypeDynamicNew $algorithmTypeDynamic;
set integratorTypeDynamicNew $integratorTypeDynamic;
set DtAnalysisNew $DtAnalysis;


set controlTime [getTime];
# set ReducedDTAna [expr $DtAnalysis/2];

test $testTypeDynamic $TolDynamic $maxNumIterDynamic 0;

#Try different tolerences
set TolReduced0 [expr $TolDynamic*1.0];
set TolReduced1 [expr $TolDynamic*10.0];
set TolReduced2 [expr $TolDynamic*100.0];
set TolReduced3 [expr $TolDynamic*1000.0];
set TolReduced4 [expr $TolDynamic*10000.0];

set icount 0; #IF icount greater than zero, then return to the initial analysis setup paramters
set alFlag 0;

#If the analyis failed before finishing the whole history
while {$controlTime < $TmaxAnalysis} {
	set controlTime [getTime];
	puts "Current Analysis Time Step is: $controlTime";
	
	#If optimal shceme works well then go back to original analysis parameter setup
	if {$icount > 0} {
		test $testTypeDynamic  $TolDynamic $maxNumIterDynamic $printFlagDynamic
		set ReducedDTAna [expr $DtAnalysis];
		set icount 0;
		if {$alFlag == 1} {
		algorithm Newton;
		set algorithmTypeDynamicNew "Newton";
		}
		
	}
	
	
    set ok [analyze 1 $DtAnalysis]; #If next step works well then no need to try!
	set DtAnalysisNew [expr $DtAnalysis];
	
	if {$ok != 0} {
		puts "Trying Newton with DtAnalysis/2"
		set ReducedDTAna [expr $DtAnalysis/2.]
		set DtAnalysisNew $ReducedDTAna;
		test $testTypeDynamic  $TolReduced0 50 2
		set ok [analyze 2 $ReducedDTAna]
		set icount [expr $icount+1]
	};      # end if ok !0
	
	#Try to find a optimal way to avoid the convergence problem
	if {$ok != 0} {
		puts "Trying Newton Linear Search with Original DtAnalysis and TolDynamic!"
		test $testTypeDynamic  $TolDynamic $maxNumIterDynamic $printFlagDynamic;
		algorithm NewtonLineSearch;
		set algorithmTypeDynamicNew "NewtonLineSearch";
		set ok [analyze 1 $DtAnalysis]
		set icount [expr $icount+1]
	}	

	if {$ok != 0} {
		puts "Trying Newton -initial with Original DtAnalysis and TolDynamic!"
		test $testTypeDynamic  $TolDynamic $maxNumIterDynamic $printFlagDynamic;
		algorithm Newton -initial;
		set algorithmTypeDynamicNew "NewtonLineSearch"
		set ok [analyze 1 $DtAnalysis]
		set icount [expr $icount+1]
	}	
	
	if {$ok != 0} {
		puts "Trying Newton with reduced tolerance .."
		test $testTypeDynamic  $TolReduced1  $maxNumIterDynamic $printFlagDynamic;
		set TolDynamicNew $TolReduced1
		set ReducedDTAna [expr $DtAnalysis]
		set ok [analyze 1 $ReducedDTAna]
		set icount [expr $icount+1]
	}
	
	if {$ok != 0} {
		puts "Trying Newton with DtAnalysis/2 and reduced1 tolerance .."
		set ReducedDTAna [expr $DtAnalysis/2.]
		set DtAnalysisNew $ReducedDTAna;
		test $testTypeDynamic  $TolReduced1 50 2
		set TolDynamicNew $TolReduced1
		set ok [analyze 2 $ReducedDTAna]
		set icount [expr $icount+1]
	};      # end if ok !0
	
	if {$ok != 0} {
		puts "Trying Newton with DtAnalysis/2 and reduced3 tolerance .."
		set ReducedDTAna [expr $DtAnalysis/2.]
		set DtAnalysisNew $ReducedDTAna;
		test $testTypeDynamic $TolReduced3 50 2
		set TolDynamicNew $TolReduced3
		set ok [analyze 2 $ReducedDTAna]
		set icount [expr $icount+1]
	};      # end if ok !0
	
	if {$ok != 0} {
		puts "Trying Newton with DtAnalysis/10 and reduced4 tolerance .."
		set ReducedDTAna [expr $DtAnalysis/10.]
		set DtAnalysisNew $ReducedDTAna;
		test $testTypeDynamic  $TolReduced4 50 2
		set TolDynamicNew $TolReduced4
		set ok [analyze 10 $ReducedDTAna]
		set icount [expr $icount+1]
	};      # end if ok !0
	
	if {$ok != 0} {
		puts "Trying Newton with DtAnalysis/50 and reduced4 tolerance .."
		set ReducedDTAna [expr $DtAnalysis/50.]
		test $testTypeDynamic  $TolReduced4 50 2
		set ok [analyze 50 $ReducedDTAna]
		set icount [expr $icount+1]
	};      # end if ok !0
	
	if {$ok != 0} {
		puts "Trying NewtonLineSearchwith DtAnalysis/50 and reduced4 tolerance .."
		set ReducedDTAna [expr $DtAnalysis/50.]
		set DtAnalysisNew $ReducedDTAna;
		test $testTypeDynamic  $TolReduced4 50 2
		set TolDynamicNew $TolReduced4
		algorithm NewtonLineSearch 0.5;
		set algorithmTypeDynamicNew "NewtonLineSearch 0.5"
		set alFlag 1;
		set ok [analyze 50 $ReducedDTAna]
		set icount [expr $icount+1]
	};      # end if ok !0
	
	# Added by Yong Li, 2012.01.18
		#Output Nodal Solution Information
		set dataDir .;
		set SolutionDataFile [open "$dataDir/SolutionDataFile.out" "a+"];
		# Output default solution set up results.
		set controlTime [getTime];
		puts $SolutionDataFile "Modified Solution Results, at $controlTime ok=$ok";
		puts $SolutionDataFile "constraints=$constraintsTypeDynamicNew";
		puts $SolutionDataFile "numberer=$numbererTypeDynamicNew";
		puts $SolutionDataFile "system=$systemTypeDynamicNew";
		puts $SolutionDataFile "test=$testTypeDynamicNew $TolDynamicNew $maxNumIterDynamicNew";
		puts $SolutionDataFile "algorithm=$algorithmTypeDynamicNew";
		puts $SolutionDataFile "integrator=$integratorTypeDynamicNew";
		puts $SolutionDataFile "analysisTimeStep=$DtAnalysisNew";
		close $SolutionDataFile;
	
	if {$ok != 0} {
		puts "oooppppsss...The structural analysis is a total disaster!"
		#Output Nodal Solution Information
		set dataDir .;
		set SolutionDataFile [open "$dataDir/SolutionDataFile.out" "a+"];
		# Output default solution set up results.
		puts $SolutionDataFile "oooppppsss...The structural analysis is a total disaster!";
		close $SolutionDataFile;
		break
		set ok 1
	};      # end if ok !0
};


#Output the analyze results for users to know: successful or not.
set IDctrlNode 1;
set IDctrlDOF 1;
set fmt1 "%s Earthquake analysis: CtrlNode %.3i, dof %.1i, Disp=%.4f %s";	# format for screen/file output of DONE/PROBLEM analysis
if {$ok != 0 } {
	puts [format $fmt1 "PROBLEM" $IDctrlNode $IDctrlDOF [nodeDisp $IDctrlNode $IDctrlDOF]]
} else {
	puts [format $fmt1 "DONE"  $IDctrlNode $IDctrlDOF [nodeDisp $IDctrlNode $IDctrlDOF]]
}
puts "Ground Motion Done. End Time: [getTime] out of $TmaxAnalysis "
puts " ---------------Done Earthquake Analysis -----------------"