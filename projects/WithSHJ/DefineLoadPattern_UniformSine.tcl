proc DefineLoadPattern_UniformSine { LoadTag GMdirection GMSineAccAmpl TPeriodSine DurationSine args} {
	##########################################################
	#  DefineLoadPattern_UniformSine $LoadTag $GMdirection  $GMSineAccAmpl  $TPeriodSine $DurationSine
	##########################################################
	# define load pattern for ground-motion analysis -- uniform-support Sine-Wave excitation, unidirectional & bidirectional
	# define nodal accelerations for uniform-support excitation
	#	   Silvia Mazzoni, 2008 (mazzoni@berkeley_NO_SPAM_.edu)
	##   LoadTag:	unique load tag
	##   GMdirection 		# lateral dof for ground motion input
	##   GMSineAccAmpl	# amplitude of input sine-wave acceleration
	##   TPeriodSine		# period of input sine-wave acceleration
	##   DurationSine		# duration of each sine-wave motion
	#
	set iDefaultValue "LoadFactor 1.0"
	foreach {Name DefaultValue} $iDefaultValue {
		set $Name $DefaultValue
	}
	# override defaults with anything specified in argument list:
	foreach {Option Value} $args {
		if {[string index $Option 0]=="-"} {;							# remove the hyphen
			set Option [string range $Option 1 end]
		}
		set $Option $Value
		if {[llength $Value]>1} {;											# if the argument is a list
			foreach {SubName SubValue} $Value {
				if {[string index $SubName 0]=="-"} {;				# remove the hyphen
					set SubName [string range $SubName 1 end]
				}
				set $SubName $SubValue
			}
		}
	}
	
   	set PI [expr 2*asin(1.0)];

	# Uniform EXCITATION: acceleration input
	set omegaSine [expr 2.*$PI/$TPeriodSine];
	set vel0 [expr $GMSineAccAmpl*(-1)/$omegaSine];
	set AccelSeries "Sine 0. $DurationSine $TPeriodSine -factor [expr $LoadFactor*$GMSineAccAmpl]"
	pattern UniformExcitation $LoadTag $GMdirection -accel $AccelSeries  -vel0 $vel0
}


