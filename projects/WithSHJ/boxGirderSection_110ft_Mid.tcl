#  Section Model for typical Box Girder Section for California HighSpeed Rail Bridge;
#  April 8th 2012. Yong Li, at University of California, San Diego;
#  Under the guidance of Jackson Thomas at Parsons Brinkerhoff;

	###################################################################################################################
	# BUILD FIBER SECTION
	###################################################################################################################
set deckUnconfinedTag 31;
set deckConfinedTag 32;
set deckPrestressTag 33;
set Ubig 1.e20; 				# a really large number

# nominal concrete compressive strength
	set fc 		[expr -6.5*$ksi];			        # ksi, CONCRETE Compressive Strength  (+Tension, -Compression)
	# set Ec 		[expr 57.0*sqrt(-$fc*1000.0)];	# ksi, Concrete Elastic Modulus, not used for concrete01 and concrete02
	
	# UNCON CONCRETE #
	set fc1U 		$fc;			    # UNCONFINED concrete (todeschini parabolic model), maximum stress
	set eps1U		-0.005;			    # strain at maximum strength of unconfined concrete
	set fc2U 		[expr 0.0*$fc1U];	# ultimate stress [expr 0.2*$fc1U]
	set eps2U		-0.01;			    # strain at ultimate stress
	set lambda 0.1;					    # ratio between unloading slope at $eps2 and initial slope $Ec
	uniaxialMaterial Concrete01 $deckUnconfinedTag $fc1U $eps1U $fc2U $eps2U; 			# build cover concrete (unconfined)

	
	# CON CONCRETE #
	set Kfc 		1.332;			    # ratio of confined to unconfined concrete strength
	set fc1C 		[expr $Kfc*$fc];	# CONFINED concrete (mander model), maximum stress (-8.658)[expr $Kfc*$fc]
	set eps1C		-0.00532;		    # strain at maximum stress (-0.00377)[expr 2.*$fc1C/$Ec]
	set fc2C 		-2.5;			    # ultimate stress (-1.7316)[expr 0.2*$fc1C]
	set eps2C 		-0.02;		        # strain at ultimate stress(-0.01884)[expr 5*$eps1C]
	# tensile-strength properties
	set ftC [expr -0.1*$fc1C];			# tensile strength +tension
	set ftU [expr -0.1*$fc1U];			# tensile strength +tension
	set Ets [expr $ftU/0.002];			# tension softening stiffness
	uniaxialMaterial Concrete02 $deckConfinedTag $fc1C $eps1C $fc2C $eps2C $lambda $ftC $Ets;	# build core concrete (confined)

	
	# STEEL #
	set Fy 		[expr 68.0*$ksi];				# ksi, STEEL yield stress
	set Es		[expr 29000.0*$ksi];			# ksi, modulus of steel
	set Bs		0.0115;				# strain-hardening ratio 
	set R0 		15;				    # control the transition from elastic to plastic branches
	set cR1		0.925;				# control the transition from elastic to plastic branches
	set cR2 	0.15;				# control the transition from elastic to plastic branches
	uniaxialMaterial Steel02 $IDreinf $Fy $Es $Bs $R0 $cR1 $cR2 1 0 1 0 [expr 189*$ksi];				# build reinforcement material
	#uniaxialMaterial MinMax $matTag $otherTag <-min $minStrain> <-max $maxStrain>
	#uniaxialMaterial MinMax $IDSteelTot $IDreinf -min -0.09 -max 0.09
	
# Define the fiber section
	section fiberSec $SecTag  {	
		# patch quad $matTag $numSubdivIJ $numSubdivJK $yI $zI $yJ $zJ $yK $zK $yL $zL
		patch quad $deckUnconfinedTag 100 1 -252 112 252 112 252 114 -252 114
		patch quad $deckUnconfinedTag 1 2 -252 106 -250 106 -250 112 -252 112
		patch quad $deckConfinedTag 13 2 -250 106 -189.875 106 -189.875 112 -250 112
		patch quad $deckConfinedTag 15 4 -189.875 106 -118.575 92 -118.575 112 -189.875 112
		patch quad $deckConfinedTag 5 4 -118.575 92  -98.5625 94 -98.5625 112 -118.575 112
		patch quad $deckConfinedTag 13 4 -98.5625 94 -36.5625 106 -36.5625 112 -98.5625 112
		patch quad $deckConfinedTag 15 2 -36.5625 106  36.5625 106 36.5625 112 -36.5625 112
		patch quad $deckConfinedTag 13 4 36.5625 106  98.5625 94 98.5625  112  36.5625 112
		patch quad $deckConfinedTag 5 4 98.5625 94 118.575 92 118.575 112 98.5625  112 
		patch quad $deckConfinedTag 15 4 118.575 92 189.875 106 189.875 112 118.575 112
		patch quad $deckConfinedTag 13 2 189.875 106 250 106 250 112 189.875 112
		patch quad $deckUnconfinedTag 1 2  250 106 252 106 252 112 250 112
		patch quad $deckUnconfinedTag 13 1 -252 104 -189.875 104 -189.875 106 -252 106
		patch quad $deckUnconfinedTag 15 1 -189.875 104 -120.575 90 -118.575 92 -189.875 106
		patch quad $deckUnconfinedTag 1 19 -105 2 -103 2 -118.575 92 -120.575 90
		patch quad $deckConfinedTag 5 7 -112.9 59 -98.5625 59 -98.5625 94  -118.575 92
		patch quad $deckUnconfinedTag 1 7 -98.5625 59 -96.5625 59 -96.5625 92 -98.5625 94
		patch quad $deckUnconfinedTag 13 1 -96.5625 92 -36.5625 104 -36.5625 106 -98.5625 94
		patch quad $deckUnconfinedTag 15 1 -36.5625 104 36.5625 104 36.5625 106 -36.5625 106
		patch quad $deckUnconfinedTag 1 19 36.5625 104 96.5625 92 98.5625 94 36.5625 106
		patch quad $deckUnconfinedTag 5 7 96.5625 59 98.5625 59 98.5625 94 96.5625 92
		patch quad $deckConfinedTag 1 7 98.5625 59 112.9 59 118.575 92 98.5625 94
		patch quad $deckUnconfinedTag 13 1 103 2 105 2 120.575 90 118.575 92
		patch quad $deckUnconfinedTag 15 1 120.575 90 189.875 104 189.875 106 118.575 92
		patch quad $deckUnconfinedTag 13 1 189.875 104 252 104 252 106 189.875 106
		patch quad $deckConfinedTag 1 7 -105.25 14 -91.375 14 -98.5625 59 -112.9 59
		patch quad $deckConfinedTag 5 7 -103 2 -91.375 2 -91.375 14 -105.25 14
		patch quad $deckUnconfinedTag 1 19 -91.375 14 -56.375 8 -56.375 10 -89.375 16 
		patch quad $deckConfinedTag 15 1 -91.375 2 -56.375 2 -56.375 8 -91.375 14
		patch quad $deckUnconfinedTag 13 1 -91.375 14 -89.375 16 -96.5625 59 -98.5625 59
		patch quad $deckUnconfinedTag 3 10 -56.375 8 56.375 8 56.375 10 -56.375 10
		patch quad $deckConfinedTag 3 3 -56.375 2 56.375 2 56.375 8 -56.375 8
		patch quad $deckUnconfinedTag 8 1 56.375 8 91.375 14 89.375 16 56.375 10
		patch quad $deckConfinedTag 8 3 56.375 2 91.375 2 91.375 14 56.375 8
		patch quad $deckConfinedTag 3 3 91.375 2 103 2 105.25 14 91.375 14
		patch quad $deckUnconfinedTag 1 10 89.375 16 91.375 14 98.5625 59 96.5625 59 
		patch quad $deckConfinedTag 3 10 91.375 14 105.25 14 112.9 59 98.5625 59
		patch quad $deckUnconfinedTag 42 1 -105 0 105 0 105 2 -105 2
		# fiber $yLoc $zLoc $A $matTag
		fiber -96 6 4.123 $deckPrestressTag;
		fiber 96 6 4.123 $deckPrestressTag;
		fiber 99 6 4.774 $deckPrestressTag;
		fiber -99 6 4.774 $deckPrestressTag;
		fiber 102 6 4.774 $deckPreStressTag;
		fiber -102 6 4.774 $deckPreStressTag; 
		fiber 101 15 4.774 $deckPreStressTag; 
		fiber -101 15 4.774 $deckPreStressTag; 
		fiber 102 20 4.774 $deckPreStressTag; 
		fiber -102 20 4.774 $deckPreStressTag; 
		
}
	
	# assign torsional Stiffness for 3D Model
	set SecTagTorsion 99;								# ID tag for torsional section behavior
	set SecTag3D 3;									# ID tag for combined behavior for 3D model
	uniaxialMaterial Elastic $SecTagTorsion $Ubig;					# define elastic torsional stiffness
	section Aggregator $SecTag3D $SecTagTorsion T -section $SecTag;			# combine section properties