set inclineOption "YES";	

set in 1;
set ksi 1;
	# Parameters for stiff element properties, need to be large enough, but not too big, otherwise, there will negative eigenvalues.
set A_inf 1;
set E_inf [expr 1.0e10*$ksi];
set v_inf [expr 0.0];
set G_inf [expr $E_inf/(2*(1+$v_inf))];
set Iy_inf 1;
set Iz_inf 1;
set J_inf 1;

set A 1;
set E 20;
set G 10;
set J 500;
set Iy 200;
set Iz 200
wipe;
model basic -ndm 3 -ndf 6;

node 1 0 0 0;
node 2 0 0 10;
node 3 -5 0 15;
node 4 5  0 15;

if {$inclineOption == "NO" } {
	node 5 -5 0 10;
	node 6 5 0 10;
};

set transfTag_Column 1;
#geomTransf PDelta $transfTag $vecxzX $vecxzY $vecxzZ <-jntOffset $dXi $dYi $dZi $dXj $dYj $dZj>
geomTransf PDelta $transfTag_Column 1 0 0;  # Caution Here for the orientation of the section.
set transfTag_RigidBar_V 2;
geomTransf Linear $transfTag_RigidBar_V 1 0 0;
set transfTag_RigidBar_H 3;
geomTransf Linear $transfTag_RigidBar_H 0 0 1;
source typicalSection.tcl;
element dispBeamColumn 1 1 2 5 3 $transfTag_Column;
#element elasticBeamColumn 1 1 2 $A $E $G $J $Iy $Iz $transfTag_Column;
if {$inclineOption == "NO" } {
	element elasticBeamColumn 2 2 5 $A_inf $E_inf $G_inf $J_inf $Iy_inf $Iz_inf $transfTag_RigidBar_H;
	element elasticBeamColumn 3 2 6 $A_inf $E_inf $G_inf $J_inf $Iy_inf $Iz_inf $transfTag_RigidBar_H;
	element elasticBeamColumn 4 3 5 $A_inf $E_inf $G_inf $J_inf $Iy_inf $Iz_inf $transfTag_RigidBar_V;
	element elasticBeamColumn 5 4 6 $A_inf $E_inf $G_inf $J_inf $Iy_inf $Iz_inf $transfTag_RigidBar_V;
} else {
	element elasticBeamColumn 2 2 3 $A_inf $E_inf $G_inf $J_inf $Iy_inf $Iz_inf $transfTag_RigidBar_H;
	element elasticBeamColumn 3 2 4 $A_inf $E_inf $G_inf $J_inf $Iy_inf $Iz_inf $transfTag_RigidBar_H;
};

fix 1 1 1 1 1 1 1;

pattern Plain 1  Linear  {
	load 3 0 0 -5 0 0 0;
	load 4 0 0 -5 0 0 0;
}

# --- static analysis ---

constraints Transformation
numberer RCM
system BandGeneral
test NormDispIncr 1.0e-6 6 1
algorithm Newton
integrator LoadControl 0.1
analysis Static
analyze 10


set d [nodeDisp 4 3] 
puts "$d"