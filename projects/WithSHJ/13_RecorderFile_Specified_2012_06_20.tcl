#This file is generated through Generate_RecorderFile.m  
#To generate OpenSees Recorder File  
#Developed by Yong Li, University of Califronia, San Diego  
#Contact: foxchameleon@gmail.com/yongli@ucsd.edu  
  
######################################################  
#Recorders for Nodes  
#recorder Node <-file $fileName> <-xml $fileName> <-binary $fileName> <-tcp $inetAddress $port> <-timeSeries $tsTag> <-time> <-node   
#$node1 $node2 ...> <-nodeRange $startNode $endNode> <-region $regionTag> -dof ($dof1 $dof2 ...) $respType  
  
# #Recorders for Nodal Displacement  
# recorder Node -file $dataDir/DispNode_Dir1.out -time -node 1 110 210 310 410 510 610 710 810 901 -dof 1   disp;  
# recorder Node -file $dataDir/DispNode_Dir2.out -time -node 1 110 210 310 410 510 610 710 810 901 -dof 2   disp;  
# recorder Node -file $dataDir/DispNode_Dir3.out -time -node 1 110 210 310 410 510 610 710 810 901 -dof 3   disp;  
# #Recorders for Nodal Velocity  
# recorder Node -file $dataDir/VelNode_Dir1.out -time -node 1 110 210 310 410 510 610 710 810 901 -dof 1   vel;  
# recorder Node -file $dataDir/VelNode_Dir2.out -time -node 1 110 210 310 410 510 610 710 810 901 -dof 2   vel;  
# recorder Node -file $dataDir/VelNode_Dir3.out -time -node 1 110 210 310 410 510 610 710 810 901 -dof 3   vel;  
# #Recorders for Nodal Acceleration  
# recorder Node -file $dataDir/AccelNode_Dir1.out -time -node 1 110 210 310 410 510 610 710 810 901 -dof 1   accel;  
# recorder Node -file $dataDir/AccelNode_Dir2.out -time -node 1 110 210 310 410 510 610 710 810 901 -dof 2   accel;  
# recorder Node -file $dataDir/AccelNode_Dir3.out -time -node 1 110 210 310 410 510 610 710 810 901 -dof 3   accel;  
# #Recorders for Nodal Reaction  
# recorder Node -file $dataDir/ReactionNodes_Dir1.out -time -node 1 101 201 301 401 501 601 701 801 901 -dof 1   reaction;  
# recorder Node -file $dataDir/ReactionNodes_Dir2.out -time -node 1 101 201 301 401 501 601 701 801 901 -dof 2   reaction;  
# recorder Node -file $dataDir/ReactionNodes_Dir3.out -time -node 1 101 201 301 401 501 601 701 801 901 -dof 3   reaction;
# recorder Node -file $dataDir/ReactionNodes_Dir4.out -time -node 1 101 201 301 401 501 601 701 801 901 -dof 4   reaction;  
# recorder Node -file $dataDir/ReactionNodes_Dir5.out -time -node 1 101 201 301 401 501 601 701 801 901 -dof 5   reaction;  
# recorder Node -file $dataDir/ReactionNodes_Dir6.out -time -node 1 101 201 301 401 501 601 701 801 901 -dof 6   reaction;    
# #Isolator Displacement Response Record  
# #Record Isolator Displacement  
# recorder Node -file $dataDir/IsolatorDisp_TopDir1.out  -time -node 35 36 135 136 235 236 331 332 333 334 435 436 535 536 631 632 633 634 735 736 835 836 935 936 -dof 1   disp;  
# recorder Node -file $dataDir/IsolatorDisp_BotDir1.out  -time -node 25 26 125 126 225 226 321 322 323 324 425 426 525 526 621 622 623 624 725 726 825 826 925 926 -dof 1   disp;  
# recorder Node -file $dataDir/IsolatorDisp_TopDir2.out  -time -node 35 36 135 136 235 236 331 332 333 334 435 436 535 536 631 632 633 634 735 736 835 836 935 936 -dof 2   disp;  
# recorder Node -file $dataDir/IsolatorDisp_BotDir2.out  -time -node 25 26 125 126 225 226 321 322 323 324 425 426 525 526 621 622 623 624 725 726 825 826 925 926 -dof 2   disp;  
    
# #Isolator Response Record  
# #Record Isolator Element Force  
# recorder Element -file $dataDir/IsolatorForce.out  -time -element 55 56 155 156 255 256 351 352 353 354 455 456 555 556 651 652 653 654 755 756 855 856 955 956 localForce;  

#########################################################################################
##################################### Structural Response ###############################
#########################################################################################

set dataDir $LCTYPE;
file mkdir $dataDir;

#Recorders for Nodal Displacement 
set string "recorder Node -file $dataDir/DispPierNode_Dir1.out -time -node";
set string [concat $string $pierTopNodeTagList];
set string [concat $string "-dof 1   disp;"];
eval $string;

set string "recorder Node -file $dataDir/DispPierNode_Dir2.out -time -node";
set string [concat $string $pierTopNodeTagList];
set string [concat $string "-dof 2   disp;"];
eval $string;

set string "recorder Node -file $dataDir/DispPierNode_Dir3.out -time -node";
set string [concat $string $pierTopNodeTagList];
set string [concat $string "-dof 3   disp;"];
eval $string;

#Recorders for Nodal Velocity 
set string "recorder Node -file $dataDir/VelPierNode_Dir1.out -time -node";
set string [concat $string $pierTopNodeTagList];
set string [concat $string "-dof 1   vel;"];
eval $string;

set string "recorder Node -file $dataDir/VelPierNode_Dir2.out -time -node";
set string [concat $string $pierTopNodeTagList];
set string [concat $string "-dof 2   vel;"];
eval $string;

set string "recorder Node -file $dataDir/VelPierNode_Dir3.out -time -node";
set string [concat $string $pierTopNodeTagList];
set string [concat $string "-dof 3   vel;"];
eval $string;

#Recorders for Nodal Acceleration  
set string "recorder Node -file $dataDir/AccelPierNode_Dir1.out -time -node";
set string [concat $string $pierTopNodeTagList];
set string [concat $string "-dof 1   accel;"];
eval $string;

set string "recorder Node -file $dataDir/AccelPierNode_Dir2.out -time -node";
set string [concat $string $pierTopNodeTagList];
set string [concat $string "-dof 2   accel;"];
eval $string;

set string "recorder Node -file $dataDir/AccelPierNode_Dir3.out -time -node";
set string [concat $string $pierTopNodeTagList];
set string [concat $string "-dof 3   accel;"];
eval $string;

#Recorders for Nodal Reaction 
set string "recorder Node -file $dataDir/ReactionNode_Dir1.out -time -node";
set string [concat $string $pierBotNodeTagList];
set string [concat $string "-dof 1   reaction;"];
eval $string;

set string "recorder Node -file $dataDir/ReactionNode_Dir2.out -time -node";
set string [concat $string $pierBotNodeTagList];
set string [concat $string "-dof 2   reaction;"];
eval $string;

set string "recorder Node -file $dataDir/ReactionNode_Dir3.out -time -node";
set string [concat $string $pierBotNodeTagList];
set string [concat $string "-dof 3   reaction;"];
eval $string;

set string "recorder Node -file $dataDir/ReactionNode_Dir4.out -time -node";
set string [concat $string $pierBotNodeTagList];
set string [concat $string "-dof 4   reaction;"];
eval $string;

set string "recorder Node -file $dataDir/ReactionNode_Dir5.out -time -node";
set string [concat $string $pierBotNodeTagList];
set string [concat $string "-dof 5   reaction;"];
eval $string;

set string "recorder Node -file $dataDir/ReactionNode_Dir6.out -time -node";
set string [concat $string $pierBotNodeTagList];
set string [concat $string "-dof 6   reaction;"];
eval $string;

#Isolator Displacement Response Record  
#Record Isolator Displacement  
set string "recorder Node -file $dataDir/IsolatorDisp_TopDir1.out -time -node";
set string [concat $string $isolatorTopNodeList];
set string [concat $string "-dof 1   disp;"];
eval $string;

set string "recorder Node -file $dataDir/IsolatorDisp_TopDir2.out -time -node";
set string [concat $string $isolatorTopNodeList];
set string [concat $string "-dof 2   disp;"];
eval $string;

set string "recorder Node -file $dataDir/IsolatorDisp_BotDir1.out -time -node";
set string [concat $string $isolatorBotNodeList];
set string [concat $string "-dof 1   disp;"];
eval $string;

set string "recorder Node -file $dataDir/IsolatorDisp_BotDir2.out -time -node";
set string [concat $string $isolatorBotNodeList];
set string [concat $string "-dof 2   disp;"];
eval $string;

#Record SHJ Displacement  
set string "recorder Node -file $dataDir/SHJNodeDisp_Dir1.out -time -node";
set string [concat $string $expansionSHJNodeList];
set string [concat $string "-dof 1   disp;"];
eval $string;

#Record abutmentGap Displacement 
set string "recorder Node -file $dataDir/AbutmentGapNodeDisp_Dir1.out -time -node";
set string [concat $string $abutmentGapNodeList];
set string [concat $string "-dof 1   disp;"];
eval $string;

# The response of the deck above the pier (connecting the isolator)
set string "recorder Node -file $dataDir/DispDeckNode4Iso_Dir1.out -time -node";
set string [concat $string $deckNodeTag4IsoList];
set string [concat $string "-dof 1   disp;"];
eval $string;

set string "recorder Node -file $dataDir/DispDeckNode4Iso_Dir2.out -time -node";
set string [concat $string $deckNodeTag4IsoList];
set string [concat $string "-dof 2   disp;"];
eval $string;

set string "recorder Node -file $dataDir/DispDeckNode4Iso_Dir3.out -time -node";
set string [concat $string $deckNodeTag4IsoList];
set string [concat $string "-dof 3   disp;"];
eval $string;

set string "recorder Node -file $dataDir/AccelDeckNode4Iso_Dir1.out -time -node";
set string [concat $string $deckNodeTag4IsoList];
set string [concat $string "-dof 1   accel;"];
eval $string;

set string "recorder Node -file $dataDir/AccelDeckNode4Iso_Dir2.out -time -node";
set string [concat $string $deckNodeTag4IsoList];
set string [concat $string "-dof 2   accel;"];
eval $string;

set string "recorder Node -file $dataDir/AccelDeckNode4Iso_Dir3.out -time -node";
set string [concat $string $deckNodeTag4IsoList];
set string [concat $string "-dof 3   accel;"];
eval $string;

# The response of the midspan
set string "recorder Node -file $dataDir/DispMidSpanNode_Dir1.out -time -node";
set string [concat $string $deckNodeTag4MidList];
set string [concat $string "-dof 1   disp;"];
eval $string;

set string "recorder Node -file $dataDir/DispMidSpanNode_Dir2.out -time -node";
set string [concat $string $deckNodeTag4MidList];
set string [concat $string "-dof 2   disp;"];
eval $string;

set string "recorder Node -file $dataDir/DispMidSpanNode_Dir3.out -time -node";
set string [concat $string $deckNodeTag4MidList];
set string [concat $string "-dof 3   disp;"];
eval $string;

# The response of the end of each span
set string "recorder Node -file $dataDir/DispEndSpanNode_Dir1.out -time -node";
set string [concat $string $spanNodeTag4EndList];
set string [concat $string "-dof 1   disp;"];
eval $string;

set string "recorder Node -file $dataDir/DispEndSpanNode_Dir2.out -time -node";
set string [concat $string $spanNodeTag4EndList];
set string [concat $string "-dof 2   disp;"];
eval $string;

set string "recorder Node -file $dataDir/DispEndSpanNode_Dir3.out -time -node";
set string [concat $string $spanNodeTag4EndList];
set string [concat $string "-dof 3   disp;"];
eval $string;

set string "recorder Node -file $dataDir/DispEndSpanNode_Dir4.out -time -node";
set string [concat $string $spanNodeTag4EndList];
set string [concat $string "-dof 4   disp;"];
eval $string; # Used for evaluate the deck Twist Diagram, ref. 12.6.4.8

# The response of the end of segmental deck
set string "recorder Node -file $dataDir/DispEndDeckNode_Dir1.out -time -node";
set string [concat $string $deckNodeTag4EndList];
set string [concat $string "-dof 1   disp;"];
eval $string; # Used for relative longitudinal displacement at expansion joints, ref. 12.6.5.2;

set string "recorder Node -file $dataDir/DispEndDeckNode_Dir2.out -time -node";
set string [concat $string $deckNodeTag4EndList];
set string [concat $string "-dof 2   disp;"];
eval $string;# Used for relative transversal displacement at expansion joints, ref. 12.6.5.4;

set string "recorder Node -file $dataDir/DispEndDeckNode_Dir3.out -time -node";
set string [concat $string $deckNodeTag4EndList];
set string [concat $string "-dof 3   disp;"];
eval $string;# Used for relative vertical displacement at expansion joints, ref. 12.6.5.3;

set string "recorder Node -file $dataDir/DispEndDeckNode_Dir4.out -time -node";
set string [concat $string $deckNodeTag4EndList];
set string [concat $string "-dof 4   disp;"];
eval $string;

set string "recorder Node -file $dataDir/DispEndDeckNode_Dir5.out -time -node";
set string [concat $string $deckNodeTag4EndList];
set string [concat $string "-dof 5   disp;"];
eval $string;

set string "recorder Node -file $dataDir/DispEndDeckNode_Dir6.out -time -node";
set string [concat $string $deckNodeTag4EndList];
set string [concat $string "-dof 6   disp;"];
eval $string;# Used for rotation about vertical axis at deck ends (local), ref. 12.6.4.7;

#########################################################################################
##################################### Rail Response ################################
#########################################################################################
# The response of rail node at the end of segmental deck 
set string "recorder Node -file $dataDir/DispEndRail_1Node_Dir1.out -time -node";
set string [concat $string $railNodeTag4DeckEndList1];
set string [concat $string "-dof 1   disp;"];
eval $string; # Used for relative longitudinal displacement at expansion joints, ref. 12.6.5.2;

set string "recorder Node -file $dataDir/DispEndRail_4Node_Dir1.out -time -node";
set string [concat $string $railNodeTag4DeckEndList4];
set string [concat $string "-dof 1   disp;"];
eval $string; # Used for relative longitudinal displacement at expansion joints, ref. 12.6.5.2;

set string "recorder Node -file $dataDir/DispEndRail_1Node_Dir2.out -time -node";
set string [concat $string $railNodeTag4DeckEndList1];
set string [concat $string "-dof 2   disp;"];
eval $string; # Used for relative longitudinal displacement at expansion joints, ref. 12.6.5.4;

set string "recorder Node -file $dataDir/DispEndRail_4Node_Dir2.out -time -node";
set string [concat $string $railNodeTag4DeckEndList4];
set string [concat $string "-dof 2   disp;"];
eval $string; # Used for relative longitudinal displacement at expansion joints, ref. 12.6.5.4;

set string "recorder Node -file $dataDir/DispEndRail_1Node_Dir3.out -time -node";
set string [concat $string $railNodeTag4DeckEndList1];
set string [concat $string "-dof 3   disp;"];
eval $string; # Used for relative longitudinal displacement at expansion joints, ref. 12.6.5.3;

set string "recorder Node -file $dataDir/DispEndRail_4Node_Dir3.out -time -node";
set string [concat $string $railNodeTag4DeckEndList4];
set string [concat $string "-dof 3   disp;"];
eval $string; # Used for relative longitudinal displacement at expansion joints, ref. 12.6.5.3;

#########################################################################################
##################################### Elemental Response ################################
#########################################################################################

#Isolator Response Record  
#Record Isolator Element Force  
set string "recorder Element -file $dataDir/IsolatorForce.out  -time -element";
set string [concat $string $isolatorEleTagList];
set string [concat $string "localForce;"];
eval $string;

#Record Isolator Element Force  
set string "recorder Element -file $dataDir/IsolatorForce.out  -time -element";
set string [concat $string $isolatorEleTagList];
set string [concat $string "localForce;"];
eval $string;

#Record Slotted Hinge Joint Element Force  
set string "recorder Element -file $dataDir/SHJForce.out  -time -element";
set string [concat $string $expansionSHJEleList];
set string [concat $string "localForce;"];
eval $string;

# Record the column bottom element force
set string "recorder Element -file $dataDir/ColumnForce.out  -time -element";
set string [concat $string $columnBotEleTagList];
set string [concat $string "globalForce;"];
eval $string;

# Record the deck (mid and end) element force
set string "recorder Element -file $dataDir/DeckForce.out  -time -element";
set string [concat $string $deckMidEndEleTagList];
set string [concat $string "globalForce;"];
eval $string;

#########################################################################################
###################################### Section Response #################################
#########################################################################################

# Section Force
set string "recorder Element -file $dataDir/ColumnSecForce.out -time -element";
set string [concat $string $columnBotEleTagList];
set string [concat $string "section"];
set string [concat $string "1"];
set string [concat $string "force"];
eval $string;

set string "recorder Element -file $dataDir/DeckSecForce.out -time -element";
set string [concat $string $deckMidEndEleTagList];
set string [concat $string "section"];
set string [concat $string "1"];
set string [concat $string "force"];
eval $string;

# Section Deformation
set string "recorder Element -file $dataDir/ColumnSecDef.out -time -element";
set string [concat $string $columnBotEleTagList];
set string [concat $string "section"];
set string [concat $string "1"];
set string [concat $string "deformation"];
eval $string;

set string "recorder Element -file $dataDir/DeckSecDef.out -time -element";
set string [concat $string $deckMidEndEleTagList];
set string [concat $string "section"];
set string [concat $string "1"];
set string [concat $string "deformation"];
eval $string;


#########################################################################################
####################################### Fiber Response ##################################
#########################################################################################