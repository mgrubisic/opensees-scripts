proc AnalyzeLoadControl_Yong {args}  {;			
	###########################################################################
	## USe (1) AnalyzeLoadControl
	##  or
	## USe (2) AnalyzeLoadControl -analysisType $analysisType -constraintsType $constraintsType -alphaSP $alphaSP -alphaMP $alphaMP -numbererType $numbererType \
	##			-systemType $systemType -testType $testType -maxNumIter $maxNumIter -printFlag $printFlag -Tolerance $Tolerance -maxNumIterConverge $maxNumIterConverge \
	## 			-printFlagConverge $printFlagConverge -algorithmType $algorithmType -NewtonLineSearchRatio $NewtonLineSearchRatio -algorithmCount $algorithmCount -Nstep $Nstep
	##  or
	## USe (3) set AnalysisData "analysisType $analysisType constraintsType $constraintsType alphaSP $alphaSP alphaMP $alphaMP  numbererType $numbererType \
	##			systemType $systemType testType $testType maxNumIter $maxNumIter printFlag $printFlag Tolerance $Tolerance maxNumIterConverge $maxNumIterConverge \
	## 			printFlagConverge $printFlagConverge algorithmType $algorithmType NewtonLineSearchRatio $NewtonLineSearchRatio algorithmCount $algorithmCount Nstep $Nstep "
	##       AnalyzeLoadControl -AnalysisData $AnalysisData
	
	############################################################################################
	# perform load-controlled static analysis (gravity) based on previously-defined load pattern
	# From Tcl Manual: If the last formal argument has the name args, then a call to the procedure may contain more actual arguments than the procedure has formals. 
	#         In this case, all of the actual arguments starting at the one that would be assigned to args are combined into a list (as if the list command had been used); 
	#         this combined value is assigned to the local variable args. 
	# by Silvia Mazzoni, 2008
	# Modified by Yong LI, foxchameleon@gmail.com, 2012.06.14, working on CHSR Bridge Project. 
	
	# input variables
	#	$Nstep	 	: number of steps to apply total load
	############################################################################################
	
	# set up a few default values. these will be over-run when they are defined as optional values at the end of the list.
	set DefaultLoadControlAnalysisModelData "analysisType Static constraintsType Transformation \
			alphaSP 1e6 alphaMP 1e6 numbererType RCM systemType BandGeneral testType EnergyIncr  maxNumIter 6 printFlag 0 Tolerance 1e-8\
			maxNumIterConverge 2000 printFlagConverge 0 algorithmType Newton NewtonLineSearchRatio 0.8 algorithmCount 5 Nstep 10"
	foreach {Name DefaultValue} $DefaultLoadControlAnalysisModelData {
		set $Name $DefaultValue;
	}
	# NOTE: check for rigid diaphragms. if they are used, you should use Lagrange Constraints. Transformation is good for large models.
	
	# -----------------------------------------------
	# Override defaults with anything specified in argument list:
	foreach {Option Value} $args {
		if {[string index $Option 0]=="-"} {;							# remove the hyphen
			set Option [string range $Option 1 end]
		}
		set $Option $Value
		
		if {[llength $Value]>1} {;											# if the argument is a list
			foreach {SubName SubValue} $Value {
				if {[string index $SubName 0]=="-"} {;				# remove the hyphen
					set SubName [string range $SubName 1 end]
				}
				set $SubName $SubValue
			}
		}
	}

	# ::: Yong Li ::::
	set algorithm1 "algorithm Newton"; #Uses the tangent at the current iteration to iterate to convergence
	set algorithm2 "algorithm Newton -initial"; #optional flag to indicate to use initial stiffness iterations.
	set algorithm3 "algorithm Newton -initialThenCurrent"; #optional flag to indicate to use initial stiffness on first step, then use current stiffness for subsequent steps.
	set algorithm4 "algorithm ModifiedNewton -initial"; #Uses the tangent at the first iteration to iterate to convergence,starting at a good initial guess U0
	set algorithm5 "algorithm NewtonLineSearch -type Bisection -tol 0.8"; #-type: Bisection, Secant, RegulaFalsi, InitialInterpolated, 
	set algorithm6 "algorithm KrylovNewton -iterate current increment current";# current, initial, noTangent
	set algorithm7 "algorithm BFGS";
	set algorithm8 "algorithm Broyden 4"; #Need number of iterations within a time step until a new tangent is formed ;
	
	
	
	# -----------------------------------------------
	set ConvergeStaticAnalysisCommands {;					# evaluate these commands to when having convergenct problems. key: the NewTolerance variable.
		 
			foreach {algorithmSetUp} {"algorithm Newton -initial" "algorithm Newton -initialThenCurrent" "algorithm ModifiedNewton -initial" \
			"algorithm NewtonLineSearch -type Bisection -tol 0.8" "algorithm KrylovNewton -iterate current increment current" "algorithm BFGS" "algorithm Broyden 4"} {
				eval $algorithmSetUp;
				puts "YONG: $algorithmSetUp is used for now!"
				set ok [analyze 1];
				if {$ok == 1} {
					break;
					};
					
				if {$ok != 1} {
					foreach {toleranceSetUp} {0.05} {
						test  $testType $toleranceSetUp $maxNumIter;
						puts "YONG: $toleranceSetUp is used for now!"
						set ok [analyze 1];
						if {$ok == 1} {
							test  $testType 0.005 $maxNumIter;
							break;
							}
					}
				}
			}
		
			# puts "Trying Newton with Initial Tangent .."
			# test $testType $NewTolerance	$maxNumIterConverge $printFlagConverge 
			# algorithm Newton -initial
			# set ok [analyze 1]
			# test $testType $Tolerance	$maxNumIter    $printFlag 
			# algorithm $algorithmType
		# if {$ok != 0} {
			# puts "Trying Broyden .."
			# algorithm Broyden 8
			# set ok [analyze 1 ]
			# algorithm $algorithmType
		# }
		
		# if {$ok != 0} {
			# puts "Trying NewtonWithLineSearch .."
			# algorithm NewtonLineSearch $NewtonLineSearchRatio 
			# set ok [analyze 1]
			# algorithm $algorithmType
		# }
		
		if {$ok == 0} {puts "Converged at this step, continuing...."}
		set returnValue $ok
	}

# -------------------------------------------------- clean analysis model and results -----------------------
	wipeAnalysis
# ---------------------------------------------------------------------------------------------------------------------

	if {$constraintsType == "Plain" | $constraintsType == "Transformation"} {;			# CONSTRAINTS
		constraints $constraintsType ;	# Plain & Transformation constraints
	} else {
		constraints $constraintsType $alphaSP $alphaMP ;	# Penalty & Lagrange congs
	}

	if {$systemType  == "SparseGeneralPivot"} {;						# SYSTEM
		system SparseGeneral  -piv;	# optional pivoting for SparseGeneral system
	} else {
		system $systemType ; 
	}
	
	numberer $numbererType;																# renumber dof's to minimize band-width (optimization), if you want to;	# NUMBERER
	test $testType $Tolerance $maxNumIter $printFlag; 										# tolerance, max no. of iterations, and print code , 1: every iteration;
	
	if {$algorithmType == "BFGS" | $algorithmType == "Broyden"	} {;					# ALGORITHM
		algorithm $algorithmType $algorithmCount;	# use Newton's solution algorithm: updates tangent stiffness at every iteration
	} elseif { $algorithmType == "NewtonLineSearch"} {
		algorithm $algorithmType $NewtonLineSearchRatio;
	} else {
		algorithm $algorithmType;											# use Newton's solution algorithm: updates tangent stiffness at every iteration
	}
	
	set dLambda1 [expr 1.0/$Nstep]
    integrator LoadControl $dLambda1 ;	
	analysis Static

	# -----------------------------------------------------------------------------------------------------------
	set ok 0;
	for {set k 1} {$k <= $Nstep} {incr k 1} {
		# ----------------------------------------------check if GUI has tried to stop the analysis------------------------
		global StopAnalysisSwitch
		update
		if {[info exist StopAnalysisSwitch]} {
			if {$StopAnalysisSwitch == "yes"} {
				return
			}
		};
		# ----------------------------------------------first analyze command------------------------
		set ok [analyze 1]
		# ----------------------------------------------if convergence failure-------------------------
		if {$ok != 0} {
			set NewTolerance $Tolerance;		# increase tolerance
			set ok [eval $ConvergeStaticAnalysisCommands];	# try some other analysis parameters
			if {$ok != 0} {
				set putout "PROBLEM with Load-Control Analysis"
				puts $putout
				return -1
			};	# end if
		}
		
		catch {DisplayAnalysisDisplays}		;			# run this proc only if it has been defined, continue w/o error otherwise
		
		
	}

	# announce if we made it over through the analysis:
	if {$ok != 0 } {
		set AnalysisResults  "PROBLEM Load-Control Analysis"
	} else {
		set AnalysisResults "DONE Load-Control Analysis"
	}
# 	puts $AnalysisResults;	# print results to screen
	return 0
}